# filename: checkboxes.rb

require_relative 'base_page'
include RSpec::Matchers

class Checkboxes < BasePage

	CHECKBOXES = { css: 'input[type = "checkbox"]' }
	
	def initialize(driver)
		super
		visit '/checkboxes'
	end

	def checkboxes_checked?
		checkboxes = @driver.find_elements(CHECKBOXES)
		expect(checkboxes.last.selected?).to eql true
	end

end