# filename: ab_testing.rb

require_relative 'base_page'

class AbTesting < BasePage

	HEADING_TEXT = { css: 'h3' }
	AB_TEST1 = 'A/B Test Control'
	AB_TEST2 = 'A/B Test Variation'
	NO_ABTEST = 'No A/B Test'

	def initialize(driver)
		super
		visit '/abtest'
	end

	def ab_text_present?
		wait_for(10) {includes?(AB_TEST1, HEADING_TEXT) || includes?(AB_TEST2, HEADING_TEXT)}
	end

	def no_ab_text_present?
		wait_for(10) {includes?(NO_ABTEST, HEADING_TEXT)}
	end

end