# filename: base_page.rb

require 'selenium-webdriver'

class BasePage

	def initialize(driver)
		@driver = driver
	end

	def refresh
		@driver.navigate.refresh
	end

	def visit(url_path)
		@driver.get ENV['public'] + ENV['base_url'] + url_path
	end

	def visit_with_auth(url_path, username, password)
		@driver.get ENV['public'] + "#{username}:#{password}@" + ENV['base_url'] + url_path
	end

	def add_cookie(name, value)
		@driver.manage.add_cookie(name: name, value: value)
	end

	def find(locator)
		@driver.find_element locator
	end

	def type(text, locator)
		find(locator).send_keys text
	end

	def submit(locator)
		find(locator).submit
	end

	def is_displayed?(locator)
		begin
			find(locator).displayed?
		rescue Selenium::WebDriver::Error::NoSuchElementError
			false
		end
	end


	def includes?(text_wanted, locator)
		text_found = find(locator).text
		text_found.include?(text_wanted)
	end


	def click(locator)
		find(locator).click
	end

	def wait_for(seconds = 15)
		Selenium::WebDriver::Wait.new(timeout: seconds).until { yield }
	end

end