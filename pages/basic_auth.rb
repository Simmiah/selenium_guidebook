# filename: basic_auth.rb

require_relative 'base_page.rb'

class BasicAuth < BasePage

	USERNAME = 'admin'
	PASSWORD = 'admin'
	SUCCESS_TEXT = { css: 'p' }
	SUCCESS_MESSAGE = 'Congratulations!'

	def initialize(driver)
		driver
		super
		visit_with_auth('/basic_auth', USERNAME, PASSWORD)
	end

	def success_message_present?
		wait_for(10) {includes?(SUCCESS_MESSAGE, SUCCESS_TEXT)}
	end

end
