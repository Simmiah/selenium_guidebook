Learning Selenium through the Dave Haeffner's [Selenium Guidebook](https://seleniumguidebook.com)

Getting Started
 - Clone this repository, use ruby version 2.1.1 and bundle install the gems.

To run
- Pass in the correct config file you wish to use.  Cloud.rb when testing using your Saucelabs account or Local.rb when testing against the deployed instance
- `rspec spec/login_spec.rb --require ./config/local.rb` or `rspec spec/login_spec.rb -r ./config/cloud.rb`
