# filename: ab_testing_spec.rb

require_relative 'spec_helper'
require_relative '../pages/ab_testing'

describe 'AB Testing' do
	
	before(:each) do
		@ab_testing = AbTesting.new(@driver)
	end

	it 'AB Testing with cookie' do
		@ab_testing.add_cookie('optimizelyOptOut', 'true')
		@ab_testing.refresh
		@ab_testing.no_ab_text_present?.should be_true
	end

	it 'AB Testing without cookie' do
		@ab_testing.ab_text_present?.should be_true
	end

end