# filename: basic_auth_spec.rb

require_relative 'spec_helper.rb'
require_relative '../pages/basic_auth.rb'

describe 'Basic Authorization' do

	before(:each) do
		@basic_auth = BasicAuth.new(@driver)
	end

	it 'Basic Authorization should work' do
		@basic_auth.success_message_present?.should be_true
	end

end