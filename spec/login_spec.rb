# filename: login_spec.rb

require_relative 'spec_helper'
require_relative '../pages/login'

describe 'Login' do

  before(:each) do
    @login = Login.new(@driver)
  end

  it 'should show login page' do
    expect(@login.login_page_present?).to be true
  end

  it 'succeeded' do
    @login.with('tomsmith', 'SuperSecretPassword!')
    expect(@login.success_message_present?).to be true
  end

  it 'failed' do
    @login.with('fail', 'fail')
    expect(@login.failure_message_present?).to be true
  end

end