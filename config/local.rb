# filename: config/local.rb

ENV['public'] ||= 'http://'
ENV['base_url'] ||= 'the-internet.herokuapp.com'
ENV['browser'] ||= 'firefox'