# filename: config/cloud.rb

require 'sauce_whisk'

ENV['public'] ||= 'http://'
ENV['base_url'] ||= 'the-internet.herokuapp.com'
ENV['host'] = 'saucelabs'
ENV['operating_system'] ||= 'Windows XP'
ENV['browser'] ||= 'internet_explorer'
ENV['browser_version'] ||= '8.0'
ENV['SAUCE_USERNAME'] = ''
ENV['SAUCE_ACCESS_KEY'] = ''
ENV['tunnel'] ||= ''

unless ENV['tunnel'].empty?
	require 'sauce'
	Sauce::Utilities::Connect.start
	ENV['base_url'] = 'http://the-internet-local:4567'
end